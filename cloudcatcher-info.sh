#!/usr/bin/env bash

#############################################
# ███████ ███████ ████████ ██    ██ ██████  #
# ██      ██         ██    ██    ██ ██   ██ #
# ███████ █████      ██    ██    ██ ██████  #
#      ██ ██         ██    ██    ██ ██      #
# ███████ ███████    ██     ██████  ██      #
#############################################

own_dir="${BASH_SOURCE%/*}/";
tmpdir="$(mktemp --directory --suffix -cloudcatcher)/";

##########################
### Colour Definitions ###
#### ANSI color codes ####
RS="\033[0m"    # reset
HC="\033[1m"    # hicolor
UL="\033[4m"    # underline
INV="\033[7m"   # inverse background and foreground
LC="\033[2m"    # locolor / dim
FBLK="\033[30m" # foreground black
FRED="\033[31m" # foreground red
FGRN="\033[32m" # foreground green
FYEL="\033[33m" # foreground yellow
FONG="\033[38;5;214m" # foreground orange
FBLE="\033[34m" # foreground blue
FMAG="\033[35m" # foreground magenta
FCYN="\033[36m" # foreground cyan
FWHT="\033[37m" # foreground white
BBLK="\033[40m" # background black
BRED="\033[41m" # background red
BGRN="\033[42m" # background green
BYEL="\033[43m" # background yellow
BBLE="\033[44m" # background blue
BMAG="\033[45m" # background magenta
BCYN="\033[46m" # background cyan
BWHT="\033[47m" # background white
##########################


###################################################################################
#  ██████  ██████  ██      ██      ███████  ██████ ████████ ██  ██████  ███    ██ #
# ██      ██    ██ ██      ██      ██      ██         ██    ██ ██    ██ ████   ██ #
# ██      ██    ██ ██      ██      █████   ██         ██    ██ ██    ██ ██ ██  ██ #
# ██      ██    ██ ██      ██      ██      ██         ██    ██ ██    ██ ██  ██ ██ #
#  ██████  ██████  ███████ ███████ ███████  ██████    ██    ██  ██████  ██   ████ #
###################################################################################

calc_reboot_required() {
	if [ -f /var/run/reboot-required ]; then
		echo -e "${HC}${FRED}*** System restart required ***${RS}";
	fi
}

package_check() {
	if [ -x "/usr/lib/update-notifier/apt-check" ]; then
		/usr/lib/update-notifier/apt-check --human-readable | head -n1;
	elif which apt-get >/dev/null 2>&1; then
		echo "$(apt-get -s -o Debug::NoLocking=true upgrade | grep -c ^Inst) packages pending upgrade.";
	else
		echo "(unknown number of packages to install - apt not present)";
	fi
}

distro_name() {
	if which lsb_release >/dev/null 2>&1; then
		lsb_release -ds | awk '{ gsub("GNU/Linux |[()]", ""); print($0); }';
	elif [ -e "/etc/redhat-release" ]; then
		cat /etc/redhat-release;
	fi
}


# Apt package pending upgrade count
package_check >"${tmpdir}apt-packages.txt" &
# Distribution name
distro_name >"${tmpdir}distro-description.txt" &

# Whether a reboot is required
calc_reboot_required >"${tmpdir}reboot_required.txt" &

# Hostname
hostname >"${tmpdir}hostname.txt" &
# Kernel version
uname -r >"${tmpdir}kernel-version.txt" &
# Uptime
uptime --pretty | sed -e 's/up //' >"${tmpdir}uptime.txt" &
# Current ips
ip addr show scope global | awk -f "${own_dir}/parse_interface_ips.awk" >"${tmpdir}ips.txt" &
# Load
cat /proc/loadavg | cut -d' ' -f 2 >${tmpdir}load.txt &

# Last login
last -1 | awk '{ print $1,"on",$5,$6,$7,"\n\tfrom",$3; exit; }' >"${tmpdir}last_login.txt" &

wait

echo -e "Welcome to $(cat ${tmpdir}hostname.txt)";
echo -e "    ${LC}running $(cat ${tmpdir}distro-description.txt)${RS}";
echo -e "";
echo -e "Kernel: ${FRED}$(cat ${tmpdir}kernel-version.txt)${RS}";
echo -e "Uptime: ${FGRN}$(cat ${tmpdir}uptime.txt)${RS}";
echo -e "Load:   ${FONG}$(cat ${tmpdir}load.txt)${RS}";
echo -e "IPs:";
echo -e "    ${FBLE}$(cat ${tmpdir}ips.txt)${RS}";
echo -e "";
echo -e "${LC}Last updated at $(date +"%l:%M%p")${RS}";
echo -e "";
cat ${tmpdir}apt-packages.txt;
cat ${tmpdir}reboot_required.txt;

### Cleanup ###

rm -r ${tmpdir};
