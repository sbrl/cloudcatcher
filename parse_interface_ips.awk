BEGIN {
	ORS = ""; # We control the output separator in END
	ip_count = 0;
}

/inet6?/ {
	gsub("/[0-9]+$", "", $2); # Remove the subnet from the end
	ips[ip_count++] = $2;
}

END {
	for(i = 0 ; i < ip_count ; i++) {
		print(ips[i]);
		if(i < ip_count - 1) print("\n    ");
	}
}
