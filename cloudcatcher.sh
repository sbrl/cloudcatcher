#!/usr/bin/env bash


### Settings ###
own_dir="${BASH_SOURCE%/*}/";

logo_filename="logo.txt";
info_filename=$(mktemp --suffix -cloudcatcher);

if [ -f "${own_dir}.cloudcatcher-settings" ]; then
	source "${own_dir}.cloudcatcher-settings";
fi

################

${own_dir}cloudcatcher-info.sh >${info_filename}

### Output ###
echo 
if [[ -f "${logo_filename}" ]]; then
	pr -mtJ "${logo_filename}" "${info_filename}";
else
	cat "${info_filename}";
fi

##############

### Cleanup ###
if [ -f "${info_filename}" ]; then
	rm "${info_filename}";
fi

###############
